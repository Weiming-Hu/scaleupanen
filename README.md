# Scaling Up Analog Ensemble

<!-- vim-markdown-toc GitLab -->

* [Introduction](#introduction)
* [Use Cases](#use-cases)
    * [Data Access](#data-access)
    * [Cheyenne](#cheyenne)
    * [OSG](#osg)

<!-- vim-markdown-toc -->

## Introduction

This repository demonstrates how to scale up [Analog Ensemble](https://weiming-hu.github.io/AnalogsEnsemble/) (AnEn) on distributed memory machine.

## Use Cases

Currently, the available use case is provided on [NCAR Cheyenne](https://www2.cisl.ucar.edu/resources/computational-systems/cheyenne).

### Data Access

The numerical weather prediction model is [NAM](https://www.ncdc.noaa.gov/data-access/model-data/model-datasets/north-american-mesoscale-forecast-system-nam). The forecast model is NAM-NMM with a 12 km resolution and the analysis data set is NAM-ANL with a 12 km resolution. Both forecasts and analysis were downloaded for the period from Oct 2008 to Aug 2019. Forecasts were downloaded with the model cycle time at 00 UTC and analysis at **all model cycle times** were downloaded.

The raw data files were compressed tar files.

### Cheyenne

Step 1: Transfer from HAS data archive to NCAR Glade file system.

`wget` is used to transfer files form HAS data archive to NCAR Glade. I created a text file, `urls.txt`, including all the file URLs by combining the server URL and the file names. And then, I can run multiple `wget` instances on NCAR Cheyenne to transfer the files to my scratch space.

```
wget -nc -v -i urls.txt

# -nc Do not overwrite existing files
# -v  Verbose information
# -i  The URLs to download
```

For each instance, the transfer speed is about 1 MB/s.

Step 2: Convert forecasts and model analysis from compressed tar files to monthly NetCDF files.

In this step, three scripts are needed.

First is the configuration file, [forecasts.cfg](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/forecasts.cfg), for `gribConverter`, that specifies what parameters to convert, how to find time and lead time information, and etc.

Second is the job script file, [batch_process.pbs](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/batch_process.pbs). This script will be executed on each computing node and it generates a monthly NetCDF file. It takes care of the following thing:

- Checking whether a month has already been or is currently being processed.
- Extracting GRIB files from compressed tar files.
- Converting from GRIB files to NetCDF files using the `forecasts.cfg` file.
- Adding wind fields to the generated NetCDF file.

Third is the job submission file, [batch_submit_process.sh](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/batch_submit_process.sh). This script submits one job a time when there is currently no queued jobs. This scheme ensures that all jobs start at different times so that they don't clash with each other, especially when deciding which folders are currently being processed and which not.

Below is a summary of consumed resources from a computing node for analysis process. This particular node has processed analysis for one month with 4 cycles, 5 forecast lead times in each cycle, and at all grid points.

```
PBS Job Id: 9383970.chadmin1.ib0.cheyenne.ucar.edu
Job Name:   process_each_month
Execution terminated
Exit_status=0
resources_used.cpupercent=98
resources_used.cput=02:16:02
resources_used.mem=10345236kb
resources_used.ncpus=1
resources_used.vmem=10414364kb
resources_used.walltime=02:19:08
```

Step 3: Convert analysis NetCDF files from the [Forecasts](https://weiming-hu.github.io/AnalogsEnsemble/2019/01/16/NetCDF-File-Types.html#forecasts) format to the [Observations](https://weiming-hu.github.io/AnalogsEnsemble/2019/01/16/NetCDF-File-Types.html#observations) format.

If you are generating forecast files, this step should be skipped. If you are using model analysis, the generated NetCDF files so far have the `Forecasts` format, we need to convert it to the `Observations` format.

This conversion is usually fast, so it can be done on a single node manually. Please see the following commands.

```
wuh20@r6i6n32:output> ls
201808.nc  201809.nc  201810.nc  201811.nc  201812.nc  201901.nc  201902.nc  201903.nc  201904.nc  201905.nc  201906.nc  201907.nc  201908.nc
wuh20@r6i6n32:output> mkdir analysis
wuh20@r6i6n32:output> for file in `ls *.nc`; do forecastsToObservations -i $file -o analysis/$file -v 3; done
Parallel Ensemble Forecasts --- Forecasts to Observations v 3.7.1
Copyright (c) 2018 Weiming Hu @ GEOlab
Converting Observations to Forecasts
Reading forecast file ...
Reading Parameters from file (201808.nc) ...
Reading dimension (num_parameters) length ...
Reading Stations from file (201808.nc) ...
Reading dimension (num_stations) length ...
```

Step 4: Generate configuration files for distributed tasks and run AnEn in parallel.

In this step, the following scripts are needed.

First, [generateConfigs.R](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/generateConfigs.R) generates distributed configurations so that each computing node can just run the `analogGenerator` command solely with this configuration file. This file requires `RAnEn` to be installed.

Second, [batch_analogGenerator.pbs](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/batch_analogGenerator.pbs) is the job submit file that execute a certain configuration file. The configuration file is given as an argument.

Third, [batch_submit_analogGenrator.sh](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/batch_submit_analogGenerator.sh) submits all `analogGenerator` tasks at once. This script creates an individual task for each configuration file.

An example resource usage log looks like the following:

```
PBS Job Id: 9435809.chadmin1.ib0.cheyenne.ucar.edu
Job Name:   analogGenerator
Execution terminated
Exit_status=0
resources_used.cpupercent=6596
resources_used.cput=18:14:35
resources_used.mem=46942232kb
resources_used.ncpus=72
resources_used.vmem=56199132kb
resources_used.walltime=00:24:15
```

An example profiling log looks like the following:

```
-----------------------------------------------------
User (CPU) Time for Analog Generator:
Total: 61525.4414062 seconds (100%)
Reading data: 112.893119812 seconds (0.183490142226%)
Computation: 61399.1367188 seconds (99.794708252%)
 -- SD: 3.58357310295 seconds (0.0058245388791%)
 -- Mapping: 3.99248504639 seconds (0.00648916093633%)
 -- Similarity: 59068.5664062 seconds (96.0067367554%)
 -- Selection: 2322.99511719 seconds (3.77566599846%)
Writing data: 13.4117441177 seconds (0.0217986963689%)
-----------------------------------------------------
-----------------------------------------------------
Real (Wall) Time for Analog Generator:
Total: 1294.65840874 seconds (100%)
Reading data: 90.5544672459 seconds (6.99446793339%)
Computation: 1192.45156189 seconds (92.1054970055%)
 -- SD: 3.48916452704 seconds (0.269504643348%)
 -- Mapping: 0.0842069401406 seconds (0.006504182074%)
 -- Similarity: 1137.13297264 seconds (87.832664197%)
 -- Selection: 51.7452177792 seconds (3.99682398307%)
Writing data: 11.6523796008 seconds (0.900035061154%)
-----------------------------------------------------
```

In total, abour 6,000 core hours are used to simulate one year of forecasts using one year of search.

Step 5: Visualize AnEn forecasts.

To visualize AnEn, we need [plotMap.R](https://gitlab.com/Weiming-Hu/scaleupanen/blob/master/Cheyenne/plotMap.R). This script can process and generate the variable prediction map for each day. It skips existing files and it is running in parallel.

A point to note is that Cheyenne batch computing node is not able to directly write figures to a file. I need to open interactive sessions to be able to write figures to files. So it is preferred to first get an interactive session with multiple processes and CPUs:

```
qsub -X -I -l select=1:ncpus=72:mpiprocs=72 -l walltime=01:00:00 -q regular -A URTG0014

# Load libraries
module purge
module load gnu gdal R ncarenv

# Run the script to generate figures
# Time this command to see how long it runs
#
time Rscript plotMap.R
```

Then, we can stack all PNG pictures as frames into a movie. The higher the frame rate is, the faster the movie.

```
# Generate AVI movie
ffmpeg -framerate 10 -pattern_type glob -i 'SurfaceTemperature_*.png' SurfaceTemperature.avi

# Convert AVI to MP4
ffmpeg -i SurfaceTemperature.avi -vcodec libx264 -acodec libfaac -vf scale=1024*768 SurfaceTemperature.mp4
```

### OSG

Below is a collection of helpful tips and links:

- Not familiar with the commands of Condor? [Monday Exercise 1.2](https://opensciencegrid.org/user-school-2019/#materials/day1/part1-ex2-commands/) shows a list of basic commands to use on Condor.
- Not sure how to start with the submit file? [Monday Exercise 1.3](https://opensciencegrid.org/user-school-2019/#materials/day1/part1-ex3-jobs/) gives a minimal version of a submit file.  
- Not sure how to use a self-built Docker image? [Wednesday Exercise 2.3](https://opensciencegrid.org/user-school-2019/#materials/day3/part2-ex3-docker/) gives an example of using a Docker image.

Step 1: Transfer data to Gluster.

Gluster is the best option for large file I/O. I have already made a request on Gluster and it is available on [learn.chtc.wisc.edu](http://learn.chtc.wisc.edu) at the following location: /mnt/gluster/s19_weiming.

The best option for transferring a copy of your data will be to use rsync to transfer files from Glade to `/mnt/gluster/s19_weiming/`. Please note, our Gluster tutorial page refers users to the transfer server, however as an OSG summer school participant I will use [learn.chtc.wisc.edu](http://learn.chtc.wisc.edu) instead of [transfer.chtc.wisc.edu](http://transfer.chtc.wisc.edu).

A few things to note (which are also stated on our Gluster tutorial page):

1. Not all compute nodes in the HTC are mounted with Gluster. This means that you will need to add the following line to you submit files: `Requirements = (Target.HasGluster == true)`. This will tell HTCondor to run your jobs on a compute node that can access Gluster. To find out how many machines match the requirement, you can use `condor_status -constraint HasGluster -constraint HasDocker`.
2. As described at step 4 and 5 of our tutorial, you will need to add some additional commands to your shell script that will copy the appropriate input files from Gluster (in your case one month of forecasts and the observation file) that are needed for that particular job. You should also remove all files copied from Gluster before your job completes (else they will be transferred back to your home directory). If the output file for your job is > 4GB you should also include commands to copy this data to Gluster before your jobs complete, and like your input files, delete this output at the end of your job.
3. More information about condor_status can be found in the [HTCondor manual](https://htcondor.readthedocs.io/en/v8_9_4/man-pages/condor_status.html).

