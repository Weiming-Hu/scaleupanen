#!/bin/bash

# Set the number of total jobs to submit
totalJobs=13

submittedJobs=0

while true; do
    number=`qstat -a -u wuh20 | grep " Q " | wc -l`
    echo The number of queued jobs: $number
    echo The number of submitted jobs: $submittedJobs
    if (( number == 0 )); then
        echo There is no queued jobs. Submit a new one.
        qsub batch_process.pbs
        submittedJobs=$((submittedJobs + 1))
        if (( submittedJobs == totalJobs )); then
            echo $submittedJobs jobs submitted. Done!
            exit 0
        fi
    fi
    sleep 10
done
