#!/bin/bash

# This script submits all configuration files
# in the current directory at once.
#

for file in `ls *.cfg`
do configFile=$file qsub batch_analogGenerator.pbs
echo configFile=$file qsub batch_analogGenerator.pbs
done

echo ""
echo "Total number of tasks: `ls *.cfg | wc -l` task(s)"
